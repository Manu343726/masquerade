#/bin/sh

mkdir build && cd build
cmake .. -DMASQUERADE_BUILD_TEST=ON -DMASQUERADE_BUILD_EXAMPLE=ON
make -j16 # amd ryzen rulz
ctest . -V
./masquerade-example
