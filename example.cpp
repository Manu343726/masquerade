/********************************************************************************
* MIT License                                                                   *
*                                                                               *
* Masquerade: C++ enum sets and bitops                                          *
*                                                                               *
* Copyright (c) 2018 Manuel Sánchez (@Manu343726)                               *
*                                                                               *
* Permission is hereby granted, free of charge, to any person obtaining a copy  *
* of this software and associated documentation files (the "Software"), to deal *
* in the Software without restriction, including without limitation the rights  *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
* copies of the Software, and to permit persons to whom the Software is         *
* furnished to do so, subject to the following conditions:                      *
*                                                                               *
* The above copyright notice and this permission notice shall be included in    *
* all copies or substantial portions of the Software.                           *
*                                                                               *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE  *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
* THE SOFTWARE.                                                                 *
********************************************************************************/

#include "masquerade.hpp"
#include <iostream>

enum class greetings
{
    HELLO, WORLD
};
MASQUERADE_ENABLE_SET(greetings);

void run(masquerade::enum_set<greetings> options)
{
    if(options.is_set(greetings::HELLO))
    {
        std::cout << "hello\n";
    }
    if(options.is_set(greetings::WORLD))
    {
        std::cout << "world\n";
    }
}

int main()
{
    run(greetings::HELLO|greetings::WORLD);
}
