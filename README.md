[![pipeline status](https://gitlab.com/Manu343726/masquerade/badges/master/pipeline.svg)](https://gitlab.com/Manu343726/masquerade/commits/master)

# masquerade

Single file header-only library to create bitmasks and option sets from C++ enums. Inspired by [this post](http://blog.bitwigglers.org/using-enum-classes-as-type-safe-bitmasks/).

## Features

 - **Single header file**: Just add `masquerade.hpp` file to your project.
 - **No external dependencies**, only a C++11 compiler is required.
 - **Enum sets**: Create a set of enum values using a natural syntax

   ``` cpp
   enum Foo { A, B };
   MASQUERADE_ENABLE_SET(Foo)

   auto options = (A | B); // "select" A and B

   if(options.is_set(A))
   {
       std::cout << "A set";
   }

   if(options.is_set(B))
   {
        std::cout << "B set";
   }
   ```
 - **Bit operations**: Automatic overloading of bit manipulation operators for enums
   
   ``` cpp
   enum Foo { A=0x1, B=0x10 };
   MASQUERADE_ENABLE_BITOPS(Foo)

   Foo value = A << 4;
   assert(value == B);
   ```

See `example.cpp` and `test.cpp` for more examples.

## License

The library and all its scripts is released under the MIT license. See LICENSE file for more info.
