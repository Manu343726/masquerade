/********************************************************************************
* MIT License                                                                   *
*                                                                               *
* Masquerade: C++ enum sets and bitops                                          *
*                                                                               *
* Copyright (c) 2018 Manuel Sánchez (@Manu343726)                               *
*                                                                               *
* Permission is hereby granted, free of charge, to any person obtaining a copy  *
* of this software and associated documentation files (the "Software"), to deal *
* in the Software without restriction, including without limitation the rights  *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
* copies of the Software, and to permit persons to whom the Software is         *
* furnished to do so, subject to the following conditions:                      *
*                                                                               *
* The above copyright notice and this permission notice shall be included in    *
* all copies or substantial portions of the Software.                           *
*                                                                               *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE  *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
* THE SOFTWARE.                                                                 *
********************************************************************************/

#ifndef MASQUERADE_HPP
#define MASQUERADE_HPP

#include <type_traits>

namespace masquerade
{

namespace detail
{

template<typename T>
struct type_tag
{
    using type = T;
};

}

}

template<typename T>
std::false_type masquerade_enum_set_enable(masquerade::detail::type_tag<T>);
template<typename T>
std::false_type masquerade_enum_bitops_enable(masquerade::detail::type_tag<T>);

namespace masquerade
{

namespace detail
{

template<typename T>
constexpr bool is_enum_set_enabled()
{
    return decltype(masquerade_enum_set_enable(type_tag<T>()))::value;
}

template<typename T>
constexpr bool is_enum_bitops_enabled()
{
    return decltype(masquerade_enum_bitops_enable(type_tag<T>()))::value;
}

template<typename Integer>
constexpr Integer zero()
{
    return static_cast<Integer>(0x0ull);
}

template<typename Integer>
constexpr Integer one()
{
    return static_cast<Integer>(0x1ull);
}

template<typename Integer>
constexpr Integer all_set()
{
    return static_cast<Integer>(-1ll);
}

template<typename Integer>
constexpr Integer set_bit(Integer value, std::size_t bit)
{
    return value | (one<Integer>() << bit);
}

template<typename Integer>
constexpr Integer unset_bit(Integer value, std::size_t bit)
{
    return value & ~(one<Integer>() << bit);
}

template<typename Integer>
constexpr bool has_bit_set(Integer value, std::size_t bit)
{
    return value & (one<Integer>() << bit);
}

}

template<typename Enum>
struct enum_set
{
    using value_type = typename std::underlying_type<Enum>::type;
    using enum_type = Enum;

    static_assert(std::is_enum<enum_type>::value,
        "Given type is not an enumeration");

#ifdef MASQUERADE_PEDANTIC
    static_assert(std::is_unsigned<value_type>::value,
        "Performing bitmask operations on signed integers has undefined behavior");
#endif // MASQUERADE_PEDANTIC

    constexpr enum_set() = default;

    constexpr explicit enum_set(const value_type value) :
        _value{value}
    {}

    constexpr enum_set(const enum_type starting_set_bit) :
        _value{masquerade::detail::set_bit(
            masquerade::detail::zero<value_type>(),
            static_cast<std::size_t>(starting_set_bit)
        )}
    {}

    constexpr enum_set set(const enum_type bit)
    {
        return enum_set{masquerade::detail::set_bit(_value, static_cast<std::size_t>(bit))};
    }

    constexpr enum_set unset(const enum_type bit)
    {
        return enum_set{masquerade::detail::unset_bit(_value, static_cast<std::size_t>(bit))};
    }

    constexpr bool is_set(const enum_type bit) const
    {
        return masquerade::detail::has_bit_set(_value, static_cast<std::size_t>(bit));
    }

    constexpr friend enum_set operator|(const enum_set lhs, const enum_set rhs)
    {
        return enum_set{lhs._value | rhs._value};
    }

    constexpr friend enum_set operator&(const enum_set lhs, const enum_set rhs)
    {
        return enum_set{lhs._value & rhs._value};
    }

    constexpr friend enum_set operator^(const enum_set lhs, const enum_set rhs)
    {
        return enum_set{lhs._value ^ rhs._value};
    }

    constexpr friend enum_set operator|(const enum_set lhs, const value_type rhs)
    {
        return lhs.set(rhs);
    }

    constexpr friend enum_set operator|(const value_type lhs, const enum_set rhs)
    {
        return rhs.set(lhs);
    }

    constexpr enum_set operator~() const
    {
        return enum_set{~_value};
    }

private:
    value_type _value = masquerade::detail::zero<value_type>();
};

template<typename Enum>
constexpr typename std::underlying_type<Enum>::type underlying_value(const Enum value)
{
    return static_cast<typename std::underlying_type<Enum>::type>(value);
}

}

template<typename Enum>
constexpr typename std::enable_if<
     masquerade::detail::is_enum_set_enabled<Enum>() &&
    !masquerade::detail::is_enum_bitops_enabled<Enum>(),
    masquerade::enum_set<Enum>
>::type operator|(const Enum lhs, const Enum rhs)
{
    return masquerade::enum_set<Enum>{lhs} | rhs;
}

#define MASQUERADE_PP_CAT_IMPL(x,y) x ## y
#define MASQUERADE_PP_CAT(x,y) MASQUERADE_PP_CAT_IMPL(x, y)


#define MASQUERADE_DECLARE_UNARY_BIT_OPERATOR(op)        \
template<typename Enum>                                  \
constexpr typename std::enable_if<                       \
    !masquerade::detail::is_enum_set_enabled<Enum>() &&  \
    masquerade::detail::is_enum_bitops_enabled<Enum>(),  \
    Enum                                                 \
>::type operator op(const Enum value)                    \
{                                                        \
    return static_cast<Enum>(                            \
        op masquerade::underlying_value(value)           \
    );                                                   \
}


#define MASQUERADE_DECLARE_BINARY_BIT_OPERATOR(op)       \
template<typename Enum>                                  \
constexpr typename std::enable_if<                       \
    !masquerade::detail::is_enum_set_enabled<Enum>() &&  \
    masquerade::detail::is_enum_bitops_enabled<Enum>(),  \
    Enum                                                 \
>::type operator op(const Enum lhs, const Enum rhs)      \
{                                                        \
    return static_cast<Enum>(                            \
        masquerade::underlying_value(lhs) op masquerade::underlying_value(rhs) \
    );                                                   \
}                                                        \
                                                         \
template<typename Enum>                                  \
constexpr typename std::enable_if<                       \
    !masquerade::detail::is_enum_set_enabled<Enum>() &&  \
    masquerade::detail::is_enum_bitops_enabled<Enum>(),  \
    Enum&                                                \
>::type operator MASQUERADE_PP_CAT(op, =)(Enum& lhs, const Enum rhs) \
{                                                        \
    return lhs = (lhs op rhs);                           \
}


#define MASQUERADE_DECLARE_BINARY_SHIFT_OPERATOR(op)       \
template<typename Enum>                                    \
constexpr typename std::enable_if<                         \
    !masquerade::detail::is_enum_set_enabled<Enum>() &&    \
    masquerade::detail::is_enum_bitops_enabled<Enum>(),    \
    Enum                                                   \
>::type operator op(const Enum lhs, const std::size_t rhs) \
{                                                          \
    return static_cast<Enum>(                              \
        masquerade::underlying_value(lhs) op rhs           \
    );                                                     \
}                                                          \
                                                           \
template<typename Enum>                                    \
constexpr typename std::enable_if<                         \
    !masquerade::detail::is_enum_set_enabled<Enum>() &&    \
    masquerade::detail::is_enum_bitops_enabled<Enum>(),    \
    Enum&                                                  \
>::type operator MASQUERADE_PP_CAT(op, =)(Enum& lhs, const std::size_t rhs) \
{                                                          \
    return lhs = (lhs op rhs);                             \
}

MASQUERADE_DECLARE_UNARY_BIT_OPERATOR(~)
MASQUERADE_DECLARE_BINARY_BIT_OPERATOR(|)
MASQUERADE_DECLARE_BINARY_BIT_OPERATOR(&)
MASQUERADE_DECLARE_BINARY_BIT_OPERATOR(^)
MASQUERADE_DECLARE_BINARY_SHIFT_OPERATOR(<<)
MASQUERADE_DECLARE_BINARY_SHIFT_OPERATOR(>>)

#define MASQUERADE_ENABLE_SET(Enum)          \
    static_assert(std::is_enum<Enum>::value, \
        "Given type is not an enum");        \
    std::true_type masquerade_enum_set_enable(masquerade::detail::type_tag<Enum>);


#define MASQUERADE_ENABLE_BITOPS(Enum)       \
    static_assert(std::is_enum<Enum>::value, \
        "Given type is not an enum");        \
    std::true_type masquerade_enum_bitops_enable(masquerade::detail::type_tag<Enum>);

#endif // MASQUERADE_HPP
