/********************************************************************************
* MIT License                                                                   *
*                                                                               *
* Masquerade: C++ enum sets and bitops                                          *
*                                                                               *
* Copyright (c) 2018 Manuel Sánchez (@Manu343726)                               *
*                                                                               *
* Permission is hereby granted, free of charge, to any person obtaining a copy  *
* of this software and associated documentation files (the "Software"), to deal *
* in the Software without restriction, including without limitation the rights  *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
* copies of the Software, and to permit persons to whom the Software is         *
* furnished to do so, subject to the following conditions:                      *
*                                                                               *
* The above copyright notice and this permission notice shall be included in    *
* all copies or substantial portions of the Software.                           *
*                                                                               *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE  *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
* THE SOFTWARE.                                                                 *
********************************************************************************/

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "masquerade.hpp"


enum class enum_set
{
    a, b, c, d
};
MASQUERADE_ENABLE_SET(enum_set)


namespace n
{
enum class enum_set
{
    a, b, c, d
};
MASQUERADE_ENABLE_SET(enum_set)
}

TEST_CASE("enum_set")
{
    SECTION("enums at global namespace are set-enabled")
    {
        masquerade::enum_set<enum_set> set = (enum_set::a|enum_set::b|enum_set::c);

        REQUIRE( set.is_set(enum_set::a));
        REQUIRE( set.is_set(enum_set::b));
        REQUIRE( set.is_set(enum_set::c));
        REQUIRE(!set.is_set(enum_set::d));
    }

    SECTION("enums at user-defined namespaces are set-enabled")
    {
        masquerade::enum_set<n::enum_set> set = (n::enum_set::a|n::enum_set::b|n::enum_set::c);

        REQUIRE( set.is_set(n::enum_set::a));
        REQUIRE( set.is_set(n::enum_set::b));
        REQUIRE( set.is_set(n::enum_set::c));
        REQUIRE(!set.is_set(n::enum_set::d));
    }

    SECTION("bitwise operations between sets")
    {
        masquerade::enum_set<n::enum_set> x = (n::enum_set::a|n::enum_set::b);
        masquerade::enum_set<n::enum_set> y = (n::enum_set::c|n::enum_set::d);

        auto x_or_y = x | y;
        auto x_and_y = x & y;
        auto x_xor_y = x ^ y;
        auto x_negated = ~x;

        REQUIRE(x_or_y.is_set(n::enum_set::a));
        REQUIRE(x_or_y.is_set(n::enum_set::b));
        REQUIRE(x_or_y.is_set(n::enum_set::c));
        REQUIRE(x_or_y.is_set(n::enum_set::d));

        REQUIRE(!x_and_y.is_set(n::enum_set::a));
        REQUIRE(!x_and_y.is_set(n::enum_set::b));
        REQUIRE(!x_and_y.is_set(n::enum_set::c));
        REQUIRE(!x_and_y.is_set(n::enum_set::d));

        REQUIRE(x_xor_y.is_set(n::enum_set::a));
        REQUIRE(x_xor_y.is_set(n::enum_set::b));
        REQUIRE(x_xor_y.is_set(n::enum_set::c));
        REQUIRE(x_xor_y.is_set(n::enum_set::d));

        REQUIRE(!x_negated.is_set(n::enum_set::a));
        REQUIRE(!x_negated.is_set(n::enum_set::b));
        REQUIRE( x_negated.is_set(n::enum_set::c));
        REQUIRE( x_negated.is_set(n::enum_set::d));
    }
}

enum class enum_bits : unsigned
{
    a = 0x1,
    b = 0x2,
    c = 0x4,
    d = 0x8
};
MASQUERADE_ENABLE_BITOPS(enum_bits);

namespace n
{
enum class enum_bits : unsigned
{
    a = 0x1,
    b = 0x2,
    c = 0x4,
    d = 0x8
};
MASQUERADE_ENABLE_BITOPS(enum_bits);
}

TEST_CASE("enum bitwise operators")
{
    SECTION("enums at global namespace are bitops-enabled")
    {
        enum_bits a_or_b  = enum_bits::a | enum_bits::b;
        enum_bits a_and_b = enum_bits::a & enum_bits::b;
        enum_bits a_xor_b = enum_bits::a ^ enum_bits::b;
        enum_bits a_negated = ~enum_bits::a;
        enum_bits a_leftshifted  = enum_bits::a << 1;
        enum_bits a_rightshifted = enum_bits::a >> 1;

        REQUIRE(a_or_b  == static_cast<enum_bits>(0x3));
        REQUIRE(a_and_b == static_cast<enum_bits>(0x0));
        REQUIRE(a_xor_b == static_cast<enum_bits>(0x3));
        REQUIRE(a_negated == static_cast<enum_bits>(~0x1));
        REQUIRE(a_leftshifted == static_cast<enum_bits>(0x1 << 1));
        REQUIRE(a_rightshifted == static_cast<enum_bits>(0x1 >> 1));
    }

    SECTION("enums at user-defined namespaces are bitops-enabled")
    {
        n::enum_bits a_or_b  = n::enum_bits::a | n::enum_bits::b;
        n::enum_bits a_and_b = n::enum_bits::a & n::enum_bits::b;
        n::enum_bits a_xor_b = n::enum_bits::a ^ n::enum_bits::b;
        n::enum_bits a_negated = ~n::enum_bits::a;
        n::enum_bits a_leftshifted  = n::enum_bits::a << 1;
        n::enum_bits a_rightshifted = n::enum_bits::a >> 1;

        REQUIRE(a_or_b  == static_cast<n::enum_bits>(0x3));
        REQUIRE(a_and_b == static_cast<n::enum_bits>(0x0));
        REQUIRE(a_xor_b == static_cast<n::enum_bits>(0x3));
        REQUIRE(a_negated == static_cast<n::enum_bits>(~0x1));
        REQUIRE(a_leftshifted == static_cast<n::enum_bits>(0x1 << 1));
        REQUIRE(a_rightshifted == static_cast<n::enum_bits>(0x1 >> 1));
    }
}
